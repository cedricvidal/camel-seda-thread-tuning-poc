package foo;

public class ExceededTimeToLiveException extends Exception {

    private static final long serialVersionUID = 8352844667127573069L;

    public ExceededTimeToLiveException() {
        super();
    }

    public ExceededTimeToLiveException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ExceededTimeToLiveException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceededTimeToLiveException(String message) {
        super(message);
    }

    public ExceededTimeToLiveException(Throwable cause) {
        super(cause);
    }

}
