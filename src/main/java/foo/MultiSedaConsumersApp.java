package foo;

import static foo.Consumer.consume;
import static org.apache.camel.ExchangePattern.InOnly;

import java.util.Random;

import org.apache.camel.Endpoint;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.main.Main;

/**
 * A Camel Application
 */
public class MultiSedaConsumersApp {

    public static class MyRouteBuilder extends RouteBuilder {
        public void configure() {
            Endpoint queue = endpoint("seda:queue?concurrentConsumers=13&multipleConsumers=true");
            from("direct:input").to(InOnly, queue);
            from(queue).routeId("seda1").bean(consume().min(2000).max(3000));
            from(queue).routeId("seda2").bean(consume().min(2000).max(3000));
        }
    }

    public static void main(String... args) throws Exception {
        final Main main = new Main();
        main.enableHangupSupport();
        main.addRouteBuilder(new MyRouteBuilder());
        main.start();
        main.getCamelContexts().get(0).setUseMDCLogging(true);

        new Thread() {
            public void run() {
                ProducerTemplate prod = null;
                try {
                    while (prod == null) {
                        Thread.sleep(1000);
                        prod = main.getCamelTemplate();
                    }
                } catch (Exception e) {
                    return;
                }
                final ProducerTemplate producer = prod;

                new Thread() {
                    Random random = new Random();

                    public void run() {
                        int j = 0;
                        while (true) {
                            producer.sendBody("direct:input", "Hello " + j++);
                            try {
                                Thread.sleep(random.nextInt(100) + 100);
                            } catch (InterruptedException e) {
                                return;
                            }
                        }
                    };
                }.start();

            };
        }.start();

        main.run(args);
    }

}
