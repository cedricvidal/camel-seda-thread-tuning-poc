package foo;

import java.util.Random;

import org.apache.camel.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer {
    Random random = new Random();
    private int min;
    private int max;

    static Logger log = LoggerFactory.getLogger(Consumer.class);

    public Consumer() {
        min = 200;
        max = 600;
    }

    @Handler
    public void handle(String message) {
        log.info("Received " + message);
        try {
            Thread.sleep(random.nextInt(max - min) + min);
        } catch (InterruptedException e) {
            return;
        }
    }

    public Consumer min(int min) {
        this.min = min;
        return this;
    }

    public Consumer max(int max) {
        this.max = max;
        return this;
    }
    
    public static Consumer consume() {
        return new Consumer();
    }

}