package foo;

import static foo.Consumer.consume;

import java.util.Random;

import org.apache.camel.Endpoint;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.main.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Camel Application
 */
public class MultiSedaApp {

    /**
     * A Camel Java DSL Router
     */
    public static class MyRouteBuilder extends RouteBuilder {

        private static Logger log = LoggerFactory.getLogger(MyRouteBuilder.class);

        private int sedaCount;

        public MyRouteBuilder(int sedaCount) {
            this.sedaCount = sedaCount;
        }

        /**
         * Let's configure the Camel routing rules using Java code...
         */
        public void configure() {
            for (int i = 0; i < sedaCount; i++) {
                Endpoint queue = endpoint("seda:queue" + i + "?concurrentConsumers=2&size=30&blockWhenFull=true");
                from("direct:input" + i).to(queue);
                from(queue).routeId("seda" + i).bean(consume().min(200).max(600));
            }
        }

    }

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
    public static void main(String... args) throws Exception {
        final Main main = new Main();
        main.enableHangupSupport();
        final int sedaCount = 1;
        main.addRouteBuilder(new MyRouteBuilder(sedaCount));

        new Thread() {
            public void run() {
                ProducerTemplate prod = null;
                try {
                    while (prod == null) {
                        Thread.sleep(1000);
                        prod = main.getCamelTemplate();
                    }
                } catch (Exception e) {
                    return;
                }
                final ProducerTemplate producer = prod;

                for (int i = 0; i < sedaCount; i++) {
                    final int sedaNb = i;
                    new Thread() {
                        Random random = new Random();

                        public void run() {
                            int j = 0;
                            while (true) {
                                producer.sendBody("direct:input" + sedaNb, "Hello " + j++);
                                try {
                                    Thread.sleep(random.nextInt(200) + 100);
                                } catch (InterruptedException e) {
                                    return;
                                }
                            }
                        };
                    }.start();
                }

            };
        }.start();

        main.run(args);
    }

}
