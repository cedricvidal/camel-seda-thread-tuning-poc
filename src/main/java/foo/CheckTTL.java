package foo;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class CheckTTL implements Processor {

    private Long maxAge = new Long(30000);

    @Override
    public void process(Exchange exchange) throws Exception {
        Long timestamp = exchange.getProperty(Exchange.CREATED_TIMESTAMP, Long.class);
        long now = System.currentTimeMillis();
        long age = now - timestamp;
        if(age > maxAge) {
            throw new ExceededTimeToLiveException();
        }
    }

    public Long getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Long maxAge) {
        this.maxAge = maxAge;
    }

    public CheckTTL maxAge(Long maxAge) {
        this.maxAge = maxAge;
        return this;
    }

}
