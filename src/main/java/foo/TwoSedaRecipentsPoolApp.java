package foo;

import static foo.Consumer.consume;
import static org.apache.camel.ExchangePattern.InOnly;

import java.util.Random;

import org.apache.camel.Endpoint;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.main.Main;

/**
 * A Camel Application
 */
public class TwoSedaRecipentsPoolApp {

    public static class MyRouteBuilder extends RouteBuilder {
        public void configure() {
            Endpoint overdueEp = endpoint("seda:overdue");
            Endpoint seda1 = endpoint("seda:queue1");
            Endpoint seda2 = endpoint("seda:queue2");

            // Entry point
            from("direct:input")
                // Send exchanges to both seda1 and seda2 in InOnly
                // This is asynchronous as those are sedas
                .to(InOnly, seda1, seda2)
                ;

            // First seda
            from(seda1).routeId("seda1")
                // Track overdue exchanges (caller should be notified in some way)
                .onException(ExceededTimeToLiveException.class).to(overdueEp).end()

                // Process exchanges with a pool
                // The maxQueueSize of the task queue is limited to avoid having a too huge buffer
                // in order to use less memory and ease monitoring
                // Messages are already queued in route's from seda
                .threads(5, 13, "seda1").id("seda1").maxQueueSize(20)

                // Check TTL. If when consumed, exchange is already past TTL, then
                // get rid of it
                .process(new CheckTTL().maxAge(10000L))

                // Actual business processing takes between 2 secs and 3 secs
                .bean(consume().min(2000).max(3000))
                ;

            // Second seda
            from(seda2).routeId("seda2")
                .onException(ExceededTimeToLiveException.class).to(overdueEp).end()
                .threads(5, 13, "seda2").id("seda2").maxQueueSize(20)
                .process(new CheckTTL().maxAge(10000L))
                .bean(consume().min(2000).max(3000));
        }
    }

    public static void main(String... args) throws Exception {
        final Main main = new Main();
        main.enableHangupSupport();
        main.addRouteBuilder(new MyRouteBuilder());
        main.start();
        main.getCamelContexts().get(0).setUseMDCLogging(true);

        new Thread() {
            public void run() {
                ProducerTemplate prod = null;
                try {
                    while (prod == null) {
                        Thread.sleep(1000);
                        prod = main.getCamelTemplate();
                    }
                } catch (Exception e) {
                    return;
                }
                final ProducerTemplate producer = prod;

                new Thread() {
                    Random random = new Random();

                    public void run() {
                        int j = 0;
                        while (true) {
                            producer.sendBody("direct:input", "Hello " + j++);
                            try {
                                Thread.sleep(random.nextInt(100) + 100);
                            } catch (InterruptedException e) {
                                return;
                            }
                        }
                    };
                }.start();

            };
        }.start();

        main.run(args);
    }

}
